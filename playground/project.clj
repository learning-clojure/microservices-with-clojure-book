(defproject playground "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main playground.core
  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resouces" "conf  "]
  :plugins [[:lein-codox "0.10.3"]
            ;; Code Coverage
            [:lein-cloverage "1.0.9"]
            ;; Unit test docs
            [test2junit "1.2.2"]]
  :codox {:namespaces :all}
  :test2junit-output-dir "target/test-reports"
  :profiles {:provided {:dependencies [[org.clojure/tools.reader "0.10.0"]
                                       [org.clojure/tools.nrepl "0.2.12"]]}
             :uberjar {:aot :all :omit-source true}
             :doc {:dependencies [[codox-theme-rdash "0.1.1"]]
                   :codox {:metadata {:doc/format :markdown}
                           :themes [:rdash]}}
             :dev {:resource-paths ["resources" "conf"]
                   :jvm-opts ["-Dconf=conf/conf.edn"]}
             :debug {:jvm-opts
                       ["-server"
                        (str "-agentlib:jdwp=transport=dt_socket,"
                             "server=y,address=8000,suspend=n")]}})
